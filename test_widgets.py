import sys
import PySide2.QtWidgets as qw
import PySide2.QtGui as qg
import PySide2.QtCore as qc

class Widget(qw.QWidget):
    def __init__(self, parent=None, *args, **kwargs):
        super(Widget, self).__init__(parent=parent)
        self.create()

    def defineThings(self, *args, **kwargs):
        ''' This is just git test. '''
        pass

    def create(self):
        layout = qw.QVBoxLayout()
        self.setLayout(layout)
        self.label = qw.QLabel()
        self.canvas = qg.QPixmap(64, 64)
        self.canvas.fill(qg.QColor(0,0,0,0))
        self.drawIt()
        self.label.setPixmap(self.canvas)

        layout.addWidget(self.label)

    def drawIt(self):
        painter = qg.QPainter(self.canvas)
        pen = qg.QPen()
        pen.setWidth(15)
        painter.setPen(pen)
        painter.drawLine(10,10, 54, 54)
        painter.drawLine(54,10, 10, 54)
        painter.end()

if __name__ == "__main__":
    app = qw.QApplication(sys.argv)
    w = Widget()
    w.show()
    
    sys.exit(app.exec_())
